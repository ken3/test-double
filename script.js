// Ken Stipek
// 7/15/2016
// TestDouble take home exercise

// To run: node script.js
// Built and tested with NodeJS 6.0.0 

// Constant for number of characters in each digit representation
const NUMBER_LENGTH = 81

// Import the test file and start tests
const fs       = require('fs');
const path     = require('path')
const filePath = path.join(__dirname, 'test-data')
input = fs.readFile(filePath, { encoding: 'utf-8' }, (_, data) => {
  runTests(data)
})

// Run tests on provided string
runTests = (data) => {
  const input = data.replace(/\n/g, '')
  const numbersCount = input.length / NUMBER_LENGTH
  for(let i = 0; i < numbersCount; i++) {
    let digits = input.substring(i * NUMBER_LENGTH, (i * NUMBER_LENGTH) + NUMBER_LENGTH)
    number = new NumberIdentifier(digits)

    // Print results for each test
    console.log(
      "Best Result: ",      number.bestResult,
      "\tFails Checksum? ", number.error, 
      "\tIllegible?",       number.illegible,
      "\tAlternatives:",    number.alternatives
    )
  }
}

class NumberIdentifier {
  constructor(characters) {
    this.numbers = {
      zero : ' _ '
           + '| |'
           + '|_|',
      one  : '   '
           + '  |'
           + '  |',
      two  : ' _ '
           + ' _|'
           + '|_ ',
      three: ' _ '
           + ' _|'
           + ' _|',
      four : '   '
           + '|_|'
           + '  |',
      five : ' _ '
           + '|_ '
           + ' _|',
      six  : ' _ '
           + '|_ '
           + '|_|',
      seven: ' _ '
           + '  |'
           + '  |',
      eight: ' _ '
           + '|_|'
           + '|_|',
      nine : ' _ '
           + '|_|'
           + ' _|'
    }
    this.characters = this._ingest(characters)
    this.result     = new Array([], [], [], [], [], [], [], [], [])
    this.error      = this.checkChecksum
    this.illegible  = false
  }

  get bestResult() {
    return this._identify(false)
  }

  // Results are stored like this when alternatives are turned on:
  // [[1,7], [1,7], [9, 8, 6], [?, 2]]
  // The first digit is a direct hit or a complete miss with the following
  // integers being likely alternatives.
  get alternatives() {
    return 'TODO' // TODO
  }

  get checkChecksum() {
    const numbers = this._identify()
    return ((this._checksum(numbers, 0) % 11) === 0)
  }

  ///////////////////////////////////
  ///////// PRIVATE METHODS /////////
  ///////////////////////////////////

  // Ingest characters into an array for easier processing
  _ingest(characters) {
    let splitCharacters = new Array('', '', '', '', '', '', '', '', '')
    for(let i = 0; i < characters.length; i++) {
      splitCharacters[Math.floor(i % 27 / 3)] += characters[i]
    }
    return splitCharacters
  }

  // Identify digits (with or without alternatives)
  _identify(includeAlts) {
    return this.characters.map((c) => {
      return this._identifyNumber(c)
    }).join('')
  }

  // Simple switch statment to identify digits
  _identifyNumber(numberString) {
    const {
      zero, one, two, three, four, five, six, seven, eight, nine
    } = this.numbers
    switch(numberString) {
      case zero:  return '0'
      case one:   return '1'
      case two:   return '2'
      case three: return '3'
      case four:  return '4'
      case five:  return '5'
      case six:   return '6'
      case seven: return '7'
      case eight: return '8'
      case nine:  return '9'
      default: {
        this.illegible = true
        return '?'  
      }
    }
  }

  // Check for alternative numbers by checking if the number of pipes or underscores
  // are within one character swap of another valid number
  _checkAlternatives(character) {
    let alts = new Array()
    const charLength = character.replace(' ', '').length
    Object.keys(this.numbers).forEach((n) => {
      let number = this.numbers[n]
      if((charLength === number.replace(' ', '').length + 1) ||
         (charLength === number.replace(' ', '').length - 1)) {
         alts.push(this._identifyNumber(number))
       }
    })
    return alts
  }

  // Recursive Checksum function
  _checksum(numbers, position) {
    return (position >= 9) ?
      (parseInt(numbers[position - 1])) :
      (position * parseInt(numbers[position - 1])) + this._checksum(numbers, position + 1)
  }
}

